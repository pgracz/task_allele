const express = require('express')
const app = express()
const fs = require('fs');
const csv = require('csv-parser')
// object of objects
const resultsObject = {}

// this is not a sane approach. could be migrated to redis (so in memory storage) or even basic sqlite would do.
// read stream from file means that if we ran more than one at the time this would break. also resources cannot be shared.
fs.createReadStream('input_tiny.vcf')
    .pipe(csv(
        {
            separator: '\t',
            // skip lines to avoid comments at the top. this is not the best way to achieve this! Sanitation should be a different step.
            skipLines: 8
        }
    ))
    // on incominig data chunk lets dispose of the ## comments
    .on('data', function (data) {
        // this is ugly. needs validation. overall needs a lot of good work to be good.
        resultsObject[data['#CHROM'] + ":" + data['POS']] = data['REF']
    })
    .on('end', () => {
        console.log("loaded the info.")
        // we have all results parsed, lets serve http server so we cna query it
        // this is just thrown together. this logic should be split into their own respective classes
        // console.log(resultsObject['chr1:16837']);

        // router and http server should become their own modules, include input validation, middleware etc.
        // also Express is apparently fading away to be replaced by koa, but i havent done any koa yet
        app.get('/query/:id', function (req, res) {

            // This is an inefficient way to look for matches, since it happens in a callback. but hey, its a POC
            if (Object.hasOwnProperty.call(resultsObject, req.params.id)) {
                res.status(200).send(resultsObject[req.params.id]);
            } else {
                res.status(404).send('Not Found! ! !');
            }

        })
        app.listen(3000);
    });


